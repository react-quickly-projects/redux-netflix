const express = require('express')
const path = require('path')
const graphqlHTTP = require('express-graphql')
const schema = require('./schema')
const {
    PORT = 3000,
    PWD = __dirname
} = process.env
const app = express()

app.get('/q', graphqlHTTP(req => ({
    schema,
    context: req.session
})))

app.use('/dist', express.static(path.resolve(PWD, 'build', 'public')))

app.get('*', (req, res) => {
    res.sendFile('index.html', {
        root: PWD
    })
})

app.listen(PORT, () => console.log(`Running server on port ${PORT}`))