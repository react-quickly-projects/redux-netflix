const React = require('react')
const styles = require('./app.css')
const { connect } = require('react-redux')

class App extends React.Component {
    render() {
        return (
            <div className={styles.app}>
                {this.props.children}
            </div>
        )
    }
}

module.exports = connect()(App)