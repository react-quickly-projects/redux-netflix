// const { createStarringList } = require('../client/components/movie/movie')
const Movie = require('../client/components/movie/movie')
// const TestUtils = require('react-dom/test-utils')
const React = require('react')
const renderer = require('react-test-renderer')
// import renderer from 'react-test-renderer'

// describe('method createStarringList', () => {
//     let l1 = [{name: 'Johnny Cash'}, {name: 'Taylor Swift'}, {name: 'Justin Bieber'}]
//     it('returns correct length of list', () => {
//     expect(createStarringList(l1)).toEqual([<li key={0}>Johnny Cash</li>, <li key={1}>Taylor Swift</li>, <li key={2}>Justin Bieber</li>])
//     })
// })
describe('starring list rendering', () => {
    let movieData = {
        title: "Avengers: Age of Ultron",
        cover: "/images/Avengers_Age_of_Ultron.jpg",
        year: "2015",
        starring: [{
            name: "Robert Downey Jr."
        }, {
            name: "Chris Hemsworth"
        }, {
            name: "Mark Ruffalo"
        }, {
            name: "Chris Evans"
        }, {
            name: "Scarlett Johansson"
        }]
    }
    // let movie = TestUtils.renderIntoDocument(<Movie params={{ id: 2 }} movie={movieData} />)
    let movie = <Movie params={{ id: 2 }} movie={movieData} />
    it('renders correctly', () => {
        const tree = renderer.create(movie).toJSON();
        expect(tree).toMatchSnapshot()
    })
})

describe('testing jest', () => {
    it('adds two numbers correctly', () => {
        expect(1 + 2).toEqual(3)
    })
})