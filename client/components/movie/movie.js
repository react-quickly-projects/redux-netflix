const React = require('react')
const PropTypes = require('prop-types')
const { connect } = require('react-redux')
const styles = require('./movie.css')
const { fetchMovieActionCreator } = require('../../modules/movies.js')
const { Link } = require('react-router')
const axios = require('axios')
const clean = require('clean-tagged-string').default

class Movie extends React.Component {
    componentWillMount() {
        this.fetchMovie(this.props.params.id)
    }

    componentWillUpdate(nextProps, nextState) {
        if(this.props.params.id !== nextProps.params.id)
            this.fetchMovie(nextProps.params.id)
    }

    isNewId(curProps, nextProps)
    {
        let curParams = curProps.params
        let nextParams = nextProps.params
        if(nextParams && !curParams)
            return true
        if(!nextParams && curParams)
            return true
        if(nextParams && curParams && nextParams.id !== curParams.id)
            return true
        return false
    }

    fetchMovie(id) {
        // FIXME: Bug with clean plugin for some reason
        // const query = clean`{
        //     movie(index:${id}) {
        //         title,
        //         cover,
        //         starring {
        //             name
        //         }
        //     }
        // }`
        const query = `{ movie(index:${id}) { title, cover, starring { name } } }`

        axios.get(`/q?query=${query}`).then(res => {
            this.props.fetchMovie(res.data.data.movie)
        })
    }

    createStarringList(starring)
    {
        return starring.map(({ name }, index) => (<li key={index}>{name}</li>))
    }

    render() {
        console.log('render')
        const {
            movie = {
                starring: []
            },
        } = this.props

        if (!movie)
            return null

        return (
            <div className={styles.movie}
                style={{ backgroundImage: `linear-gradient(90deg, rgba(0, 0, 0, 1) 0%, rgba(0, 0, 0, 0.625) 100%), url(/dist${movie.cover})`}}>
                <div>
                    <h1>{movie.title}</h1>
                    <h2> Starring:  </h2>
                    <ul> {this.createStarringList(movie.starring)} </ul>
                </div>
                <img className={styles.cover} src={`/dist${movie.cover}`} />
                <Link className={styles.exitButton}
                    to="/movies"
                >
                    X
                    </Link>
            </div>
        )
    }
}

// Movie.propTypes = {
//     params: new PropTypes{
//         id: PropTypes.number
//     }
// }

module.exports = connect(
    ({ movies }) => ({ movies: movies.all, movie: movies.current }),
    { fetchMovie: fetchMovieActionCreator }
)(Movie)
// module.exports = Movie