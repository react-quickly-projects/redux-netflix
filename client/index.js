const React = require('react')
const { render } = require('react-dom')
const { Provider } = require('react-redux')
const { createStore } = require('redux')
const reducers = require('./modules')
const routes = require('./routes')

module.exports = render((
    //Injects data from the store into children components
    <Provider store={createStore(reducers)}>
        {routes}
    </Provider>
), document.getElementById('app'))