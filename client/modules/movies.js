const { handleActions } = require('redux-actions')

const FETCH_MOVIES = 'movies/FETCH_MOVIES'
const FETCH_MOVIE = 'movies/FETCH_MOVIE'

const initialState = {
    movies: [],
    movie: {}
}

module.exports = {
    fetchMoviesActionCreator: (movies) => ({
        type: FETCH_MOVIES,
        movies
    }),
    fetchMovieActionCreator: (movie) => ({
        type: FETCH_MOVIE,
        movie
    }),
    reducer: handleActions({
        [FETCH_MOVIES]: (state, action) => ({
            ...state,
            all: action.movies
        }),
        [FETCH_MOVIE]: (state, action) => ({
            ...state,
            current: action.movie
        })
    }, initialState)
}