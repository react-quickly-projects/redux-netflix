// import React, { Component } from 'react'
const React = require('react')
const { connect } = require('react-redux')
const { fetchMoviesActionCreator } = require('../../modules/movies.js')
const { Link } = require('react-router')
const styles = require('./movies.css')
const axios = require('axios')
const clean = require('clean-tagged-string').default

class Movies extends React.Component {
    componentWillMount() {
        this.fetchMovies()
    }
    

    fetchMovies() {
        const query = clean`{
            movies {
                title,
                cover
            }
        }`

        axios.get(`/q?query=${query}`)
            .then(res => {
                this.props.fetchMovies(res.data.data.movies)
            })
    }

    render() {
        const {
            children,
            movies = [],
            params = {}
        } = this.props

        return (
            <div className={styles.movies}>
                <div className={params.id ? styles.listHidden : styles.list}>
                    {movies.map((movie, index) => (
                        <Link
                            key={index}
                            to={`/movies/${index + 1}`}>
                            <div
                                className={styles.movie}
                                style={{ backgroundImage: `url(/dist${movie.cover})` }} />
                        </Link>
                    ))}
                </div>
                {children}
            </div>
        )
    }
}

module.exports = connect(
    ({ movies }) => ({ movies: movies.all }),
    { fetchMovies: fetchMoviesActionCreator }
)(Movies)